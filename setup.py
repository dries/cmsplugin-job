from setuptools import setup, find_packages
import os

import cmsplugin_job

CLASSIFIERS = [
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
]

setup(
    name='cmsplugin-job',
    version=cmsplugin_job.get_version(),
    description='This is a job app/plugin for django-cms 2.2',
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.rst')).read(),
    author='Dries Desmet',
    author_email='dries@urga.be',
    url='https://bitbucket.org/dries/cmsplugin-job',
    packages=find_packages(),
    package_data={
        'cmsplugin_job': [
            'static/cmsplugin_job/*',
            'locale/*/LC_MESSAGES/*',
        ]
    },
    classifiers=CLASSIFIERS,
    test_suite = "cmsplugin_job.test.run_tests.run_tests",
    include_package_data=True,
    zip_safe=False,
    install_requires=['django-cms', 'simple-translation', ],
)
