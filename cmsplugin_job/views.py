import datetime
try: # pragma: no cover
    from django.views.generic.dates import BaseDateDetailView, ArchiveIndexView, _date_lookup_for_field, _date_from_string
    from django.views.generic.detail import SingleObjectTemplateResponseMixin
except ImportError: # pragma: no cover
    from cbv.views.detail import SingleObjectTemplateResponseMixin
    from cbv.views.dates import BaseDateDetailView, ArchiveIndexView, _date_lookup_for_field, _date_from_string

from django.views.generic import ListView, DetailView

from django.http import Http404
from django.shortcuts import redirect

from cms.middleware.multilingual import has_lang_prefix
from menus.utils import set_language_changer

from simple_translation.middleware import filter_queryset_language
from simple_translation.utils import get_translation_filter, get_translation_filter_language
from cmsplugin_job.models import Job, JobTitle
from cmsplugin_blog.utils import is_multilingual

class JobDetailView(DetailView):
    slug_field = get_translation_filter(Job, slug=None).items()[0][0]
    queryset = Job.objects.all()

class JobListView(ListView):
    queryset = Job.objects.all()
    paginate_by = 15
