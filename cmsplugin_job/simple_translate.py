from cmsplugin_job.models import Job, JobTitle
from simple_translation.translation_pool import translation_pool

translation_pool.register_translation(Job, JobTitle)

