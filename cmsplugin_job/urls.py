from django.conf.urls.defaults import *

from cmsplugin_job.models import Job
from cmsplugin_job.views import JobDetailView, JobListView # Both are classed based views

job_index = JobListView.as_view()

job_info_dict = {
    'queryset': Job.objects.all(),
    'date_field': 'pub_date',
    'allow_empty': True,
    'paginate_by': 15,
}

job_info_detail_dict = {
	'queryset': Job.objects.all(),
    'date_field': 'pub_date',
    'allow_empty': True,
    'slug_field': 'jobtitle__slug'
}



urlpatterns = patterns('',
    (r'^$', JobListView.as_view(), job_info_dict, 'job_index'),
    
    (r'^(?P<slug>[-\w]+)/$', JobDetailView.as_view(), {}, 'job_detail'),
    
)
