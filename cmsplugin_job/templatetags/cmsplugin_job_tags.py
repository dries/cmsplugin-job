import copy
from django.conf import settings
from django import template

from cms.models import Placeholder

register = template.Library()

@register.filter
def choose_placeholder(placeholders, placeholder):
    try:
        return placeholders.get(slot=placeholder)
    except Placeholder.DoesNotExist:
        return None


@register.inclusion_tag('admin/cmsplugin_blog/admin_helpers.html', takes_context=True)
def admin_helpers(context):
    context = copy.copy(context)
    context.update({
        'use_missing': 'missing' in settings.INSTALLED_APPS,
    })
    return context