from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.utils import get_language_from_request

from simple_translation.utils import get_translation_filter_language

from cmsplugin_job.models import LatestJobsPlugin, Job

class CMSLatestJobsPlugin(CMSPluginBase):
    """
        Plugin class for the latest jobs
    """
    model = LatestJobsPlugin
    name = _('Published jobs')
    render_template = "cmsplugin_job/latest_jobs_plugin.html"
    
    def render(self, context, instance, placeholder):
        """
            Render the latest entries
        """
        qs = Job.published.all()
        
        if instance.current_language_only:
            language = get_language_from_request(context["request"])
            kw = get_translation_filter_language(Job, language)
            qs = qs.filter(**kw)
            
        latest = qs[:instance.limit]
        
        context.update({
            'instance': instance,
            'latest': latest,
            'object_list': latest,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(CMSLatestJobsPlugin)
