CMSPLUGIN_JOB
=============
A simple django-cms plugin meant to publish a few vacancies. It's based on fivethreeo's cmsplugin-blog, which you can find here: https://github.com/fivethreeo/cmsplugin-blog
All credit must go to him, I merely renamed the code and ripped out backwards compatibility for django 1.2 and earlier (before class based views).

I put it here since somebody else might need exactly this plugin if they already use django-cms and want a quick way to post a few vacancies on a company website.

Regards,

Dries Desmet.
